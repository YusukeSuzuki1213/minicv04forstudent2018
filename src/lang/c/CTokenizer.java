package lang.c;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import lang.*;

public class CTokenizer extends Tokenizer<CToken, CParseContext> {
	@SuppressWarnings("unused")
	private CTokenRule	rule;
	private int			lineNo, colNo;
	private char		backCh;
	private boolean		backChExist = false;

	public CTokenizer(CTokenRule rule) {
		this.rule = rule;
		lineNo = 1; colNo = 1;
	}

	private InputStream in;
	private PrintStream err;

	private char readChar() {
		char ch;
		if (backChExist) {
			ch = backCh;
			backChExist = false;
		} else {
			try {
				ch = (char) in.read();
			} catch (IOException e) {
				e.printStackTrace(err);
				ch = (char) -1;
			}
		}
		++colNo;
		if (ch == '\n')  { colNo = 1; ++lineNo; }
//		System.out.print("'"+ch+"'("+(int)ch+")");
		return ch;
	}
	private void backChar(char c) {
		backCh = c;
		backChExist = true;
		--colNo;
		if (c == '\n') { --lineNo; }
	}

	// 現在読み込まれているトークンを返す
	private CToken currentTk = null;
	public CToken getCurrentToken(CParseContext pctx) {
		return currentTk;
	}
	// 次のトークンを読んで返す
	public CToken getNextToken(CParseContext pctx) {
		in = pctx.getIOContext().getInStream();
		err = pctx.getIOContext().getErrStream();
		currentTk = readToken();
//		System.out.println("Token='" + currentTk.toString());
		return currentTk;
	}
	private CToken readToken() {
		CToken tk = null;
		char ch;
		int  startCol = colNo;
		StringBuffer text = new StringBuffer();

		int state = 0;
		boolean accept = false;
		while (!accept) {
			switch (state) {
				case 0:					// 初期状態
					ch = readChar();
					if (ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r') {
					} else if (ch == (char) -1) {	// EOF
						startCol = colNo - 1;
						state = 1;
					} else if (ch >= '1' && ch <= '9') {
						startCol = colNo - 1;
						text.append(ch);
						state = 3;
					} else if (ch == '0') {
                        startCol = colNo - 1;
                        text.append(ch);
                        state = 14;
                    } else if (ch == '+') {
						startCol = colNo - 1;
						text.append(ch);
						state = 4;
					} else if (ch == '-') {
						startCol = colNo - 1;
						text.append(ch);
						state = 5;
					} else if (ch == '/') {
						startCol = colNo - 1;
						text.append(ch);
						state = 6;
					} else if (ch == '&') {
						startCol = colNo - 1;
						text.append(ch);
						state = 13;
					} else if (ch == '*') {
						startCol = colNo - 1;
						text.append(ch);
						state = 20;
					} else if (ch == '(') {
						startCol = colNo - 1;
						text.append(ch);
						state = 21;
					} else if (ch == ')') {
						startCol = colNo - 1;
						text.append(ch);
						state = 22;
					}else if (ch == '[') {
						startCol = colNo - 1;
						text.append(ch);
						state = 23;
					}else if (ch == ']') {
						startCol = colNo - 1;
						text.append(ch);
						state = 24;
					}else if ( ch >='A' && ch <= 'Z' || ch >='a' && ch <= 'z') {
						startCol = colNo - 1;
						text.append(ch);
						state = 25;
					}else {			// ヘンな文字を読んだ
						startCol = colNo - 1;
						text.append(ch);
						state = 2;
					}
					break;
				case 1:					// EOFを読んだ
					tk = new CToken(CToken.TK_EOF, lineNo, startCol, "end_of_file");
					accept = true;
					break;
				case 2:					// ヘンな文字を読んだ
					tk = new CToken(CToken.TK_ILL, lineNo, startCol, text.toString());
					accept = true;
					break;
				case 3:					// 数（10進数）の開始
					ch = readChar();
					if (Character.isDigit(ch)) {
						text.append(ch);
					} else {
						// 数の終わり
						backChar(ch);	// 数を表さない文字は戻す（読まなかったことにする）
						tk = new CToken(CToken.TK_DECIMAL, lineNo, startCol, text.toString());
						accept = true;
					}
					break;
				case 4:					// +を読んだ
					tk = new CToken(CToken.TK_PLUS, lineNo, startCol, "+");
					accept = true;
					break;
				case 5:                 //-を読んだ
					tk = new CToken(CToken.TK_MINUS, lineNo, startCol, "-");
					accept = true;
					break;
				case 6:                 // /を読んだ
					ch = readChar();
					if (ch == '/') {
						text.deleteCharAt(text.length()-1);//コメントなので/を消す（要検討
						state = 7;
					} else if (ch == '*') {
						text.deleteCharAt(text.length()-1);//コメントなので/を消す
						state = 10;
					} else if (ch >= '1' && ch <= '9' || ch >='A' && ch <= 'F' || ch >='a' && ch <= 'f' ||ch == '-' || ch == '+' || ch == '('  || ch == ')' ) {
						backChar(ch);
						state = 19;
					} else {
						backChar(ch);
						accept = true;
					}
					break;
				case 7:                     // /を読んだ
					ch = readChar();
					if (ch == '\n') {
						state = 0;
					} else if (ch == (char) -1) {
						state = 8;
					}
					break;
				case 8:
					tk = new CToken(CToken.TK_EOF, lineNo, startCol, "end_of_file");
					accept = true;
					break;
				case 9: //コメントのEOF
					System.out.println("コメントが閉じられていません");
					tk = new CToken(CToken.TK_EOF, lineNo, startCol, "end_of_file");
					accept = true;
					break;
				case 10:
					ch = readChar();
					if(ch == (char) -1) {    // EOF
						state = 9;
					} else if (ch == '*'){
						state = 11;
					}
					break;
				case 11:
					ch = readChar();
					if(ch == '/'){
						state = 0;
					} else if(ch == '*'){
						//text.append(ch);
						state = 11;
					} else if(ch == (char) -1) {   //EOF
						state = 9;
					} else {
						state = 10;
					}
					break;
				case 13:
					tk = new CToken(CToken.TK_AMPERSAND, lineNo, startCol, "&");
					accept = true;
					break;
                case 14:
					ch = readChar();
					if(ch == 'x'){
						text.append(ch);
						state = 15;
					} else if(ch >= '0' && ch <= '7'){
						text.append(ch);
						state = 16;
					} else {
						backChar(ch);
						tk = new CToken(CToken.TK_OCTAL, lineNo, startCol, text.toString());
						accept = true;
					}
					break;
				case 15:
					ch = readChar();
					if (ch >= '0' && ch <= '9' || ch >='A' && ch <= 'F' || ch >='a' && ch <= 'f' ) {
						text.append(ch);
						state = 17;
					} else {
						state = 18;
					}
					break;
				case 16:
					ch = readChar();
					if (ch >= '0' && ch <= '7') {
						text.append(ch);
					} else {
						// 数の終わり
						backChar(ch);	// 数を表さない文字は戻す（読まなかったことにする）
						tk = new CToken(CToken.TK_OCTAL, lineNo, startCol, text.toString());
						accept = true;
					}
					break;
				case 17:
					ch = readChar();
					if (ch >= '0' && ch <= '9' || ch >='A' && ch <= 'F' || ch >='a' && ch <= 'f') {
						text.append(ch);
					} else {
						// 数の終わり
						backChar(ch);	// 数を表さない文字は戻す（読まなかったことにする）
						tk = new CToken(CToken.TK_HEXADECIMAL, lineNo, startCol, text.toString());
						accept = true;
					}
					break;
				case 18:
					// ヘンな文字を読んだ
					tk = new CToken(CToken.TK_ILL, lineNo, startCol, text.toString());
					accept = true;
					break;
				case 19:
					tk = new CToken(CToken.TK_DIV, lineNo, startCol, "/");
					accept = true;
					break;
				case 20:
					tk = new CToken(CToken.TK_MULT, lineNo, startCol, "*");
					accept = true;
					break;
				case 21:
					tk = new CToken(CToken.TK_LPAR, lineNo, startCol, "(");
					accept = true;
					break;
				case 22:
					tk = new CToken(CToken.TK_RPAR, lineNo, startCol, ")");
					accept = true;
					break;
				case 23:
					tk = new CToken(CToken.TK_LBRA, lineNo, startCol, "[");
					accept = true;
					break;
				case 24:
					tk = new CToken(CToken.TK_RBRA, lineNo, startCol, "]");
					accept = true;
					break;
				case 25:
					ch = readChar();
					if (ch >= '0' && ch <= '9' || ch >='A' && ch <= 'Z' || ch >='a' && ch <= 'z') {
						text.append(ch);
//					} else if(ch == (char) -1) {
//						backChar(ch);
//						state = 26;
					}else {
						// 識別子の終わり
						backChar(ch);	// 識別子を表さない文字は戻す（読まなかったことにする）
						tk = new CToken(CToken.TK_IDENT, lineNo, startCol, text.toString());
						accept = true;
					}
					break;
				case 26:
					System.out.println("識別子で終わってね？");
					tk = new CToken(CToken.TK_EOF, lineNo, startCol, "end_of_file");
					accept = true;
					break;
			}
		}
		return tk;
	}
}
