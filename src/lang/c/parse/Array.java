package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Array extends CParseRule {
    private CToken lbra,tk;
    private CParseRule expression;
    public Array(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_LBRA;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        lbra = ct.getCurrentToken(pcx);// '['が取り出される

        tk = ct.getNextToken(pcx);//expressionが取り出さる

        if (Expression.isFirst(tk)) {
            expression = new Expression(pcx);
            expression.parse(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'['の後ろはexpressinです");
        }

        CToken ntk = ct.getCurrentToken(pcx);// 次の']'が取り出される

        if (ntk.getType() == CToken.TK_RBRA) {
            ct.getNextToken(pcx);
        } else {
            pcx.fatalError(ntk.toExplainString() + "expressinの後ろは ']' です");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (expression != null) {
            expression.semanticCheck(pcx);
            if (expression.getCType() != CType.getCType(CType.T_int) ) {
                pcx.fatalError(tk.toExplainString()+"配列の要素数はint型です。");
            }
            this.setCType(expression.getCType());
            this.setConstant(expression.isConstant());
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; Array starts");
        //if(expression != null && ident != null){
        if(expression != null ){
            expression.codeGen(pcx);
            o.println("\tMOV\t-(R6), R0\t; Array: 配列の添字を取り出す<" + lbra.toExplainString() + ">"); //添字取り出し
            o.println("\tMOV\t-(R6), R1\t; Array: 変数アドレス取り出し"); //アドレス計算
            o.println("\tADD\tR1, R0\t; Array: 変数アドレスと添字を足す");
            o.println("\tMOV\tR0, (R6)+\t; Array: 計算したアドレスをスタックに積む");
        }
        o.println(";;; Array completes");
    }
}
