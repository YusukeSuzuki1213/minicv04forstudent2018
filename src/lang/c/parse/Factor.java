package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Factor extends CParseRule {
	// factor ::= factorAmp | factor
	private CParseRule factor;
	public Factor(CParseContext pcx) {
	}
	public static boolean isFirst(CToken tk) {
		return PlusFactor.isFirst(tk) || MinusFactor.isFirst(tk) || UnsignedFactor.isFirst(tk);
	}
	public void parse(CParseContext pcx) throws FatalErrorException {
		// ここにやってくるときは、必ずisFirst()が満たされている
		CTokenizer ct = pcx.getTokenizer();
		CToken tk = ct.getCurrentToken(pcx);

		if (PlusFactor.isFirst(tk)) {
			factor = new PlusFactor(pcx);
		} else if (MinusFactor.isFirst(tk)){
			factor = new MinusFactor(pcx);
		} else if (UnsignedFactor.isFirst(tk)){
			factor = new UnsignedFactor(pcx);
		}else {
			pcx.fatalError("Factorクラスでエラー");
		}
		factor.parse(pcx);
	}

	public void semanticCheck(CParseContext pcx) throws FatalErrorException {
		if (factor != null) {
			factor.semanticCheck(pcx);
			setCType(factor.getCType());		// factor の型をそのままコピー
			setConstant(factor.isConstant());
		}
	}

	public void codeGen(CParseContext pcx) throws FatalErrorException {
		PrintStream o = pcx.getIOContext().getOutStream();
		o.println(";;; factor starts");
		if (factor != null) { factor.codeGen(pcx); }
		o.println(";;; factor completes");
	}
}

class PlusFactor extends CParseRule {
	private CToken op;
	private CParseRule plusFactor;
	public PlusFactor(CParseContext pcx) {
	}
	public static boolean isFirst(CToken tk) {
		return tk.getType() == CToken.TK_PLUS;
	}
	public void parse(CParseContext pcx) throws FatalErrorException {
		CTokenizer ct = pcx.getTokenizer();
		op = ct.getCurrentToken(pcx);
		// +の次の字句を読む
		CToken tk = ct.getNextToken(pcx);
		if (UnsignedFactor.isFirst(tk)) {
			plusFactor = new UnsignedFactor(pcx);
			plusFactor.parse(pcx);
		} else {
			pcx.fatalError(tk.toExplainString() + "+の後ろはunsignedFactorです");
		}
	}
	public void semanticCheck(CParseContext pcx) throws FatalErrorException {
		if (plusFactor != null) {
			plusFactor.semanticCheck(pcx);
			if (plusFactor.getCType().getType() == CType.T_pint) {
				// ポインタの前に'+'が付いていたらエラー
				setCType(CType.getCType(CType.T_err));
			} else {
				// そうでなければ型を引き継ぐ
				setCType(plusFactor.getCType());
			}
			setConstant(plusFactor.isConstant());
		}
	}
	public void codeGen(CParseContext pcx) throws FatalErrorException {
		PrintStream o = pcx.getIOContext().getOutStream();
		o.println(";;; plusFactor starts");
		if (plusFactor != null) { plusFactor.codeGen(pcx); }
		o.println(";;; plusFactor completes");
	}

}

class MinusFactor extends CParseRule {
	private CToken op;
	private CParseRule minusFactor;
	public MinusFactor(CParseContext pcx) {

	}
	public static boolean isFirst(CToken tk) {
		return tk.getType() == CToken.TK_MINUS;
	}
	public void parse(CParseContext pcx) throws FatalErrorException {
		CTokenizer ct = pcx.getTokenizer();
		op = ct.getCurrentToken(pcx);
		// -の次の字句を読む
		CToken tk = ct.getNextToken(pcx);
		if (UnsignedFactor.isFirst(tk)) {
			minusFactor = new UnsignedFactor(pcx);
			minusFactor.parse(pcx);
		} else {
			pcx.fatalError(tk.toExplainString() + "-の後ろはunsignedFactorです");
		}
	}
	public void semanticCheck(CParseContext pcx) throws FatalErrorException {
		if (minusFactor != null) {
			minusFactor.semanticCheck(pcx);
			if (minusFactor.getCType().getType() == CType.T_pint) {
				// ポインタの前に'-'が付いていたらエラー
				setCType(CType.getCType(CType.T_err));
				pcx.fatalError("&の前に-はいけません。");
			} else {
				// そうでなければ型を引き継ぐ
				setCType(minusFactor.getCType());
			}
			setConstant(minusFactor.isConstant());
		}
	}
	public void codeGen(CParseContext pcx) throws FatalErrorException {
		PrintStream o = pcx.getIOContext().getOutStream();
		o.println(";;; MinusFactor starts");
		if (minusFactor!= null) {
			minusFactor.codeGen(pcx);
			/* 符号反転 */
			/* スタックトップから値を取り出し、その値を0 から引いた結果をスタックに積む */
			o.println("\tMOV\t-(R6), R0\t; MinusFactor:");
			o.println("\tMOV\t#0, R1 \t; MinusFactor: 符号反転");// R1に0を格納
			o.println("\tSUB\tR0, R1 \t; MinusFactor:");
			o.println("\tMOV\tR1, (R6)+\t; MinusFactor: 符号反転の結果を積む");
		}
		o.println(";;; MinusFactor completes");
	}

}

class UnsignedFactor extends CParseRule {
	private CParseRule unsignedFactor;
	public UnsignedFactor(CParseContext pcx) {

	}
	public static boolean isFirst(CToken tk) {
		return FactorAmp.isFirst(tk) || Number.isFirst(tk) || tk.getType() == CToken.TK_LPAR || AddressToValue.isFirst(tk);
	}
	public void parse(CParseContext pcx) throws FatalErrorException {
		CTokenizer ct = pcx.getTokenizer();
		CToken tk = ct.getCurrentToken(pcx);

		if (FactorAmp.isFirst(tk)) {
			unsignedFactor = new FactorAmp(pcx);
			unsignedFactor.parse(pcx);
		} else if (Number.isFirst(tk)) {
			unsignedFactor = new Number(pcx);
			unsignedFactor.parse(pcx);
		} else if (tk.getType() == CToken.TK_LPAR) {
			CToken ntk = ct.getNextToken(pcx);
			if (Expression.isFirst(ntk)) {
				unsignedFactor = new Expression(pcx);
				unsignedFactor.parse(pcx);
			} else {
				pcx.fatalError(ntk.toExplainString() + "(の後ろはexpressionです");
			}
			CToken nntk = ct.getCurrentToken(pcx);
			if (nntk.getType() == CToken.TK_RPAR) {
				pcx.getTokenizer().getNextToken(pcx);
			} else {
				pcx.fatalError(nntk.toExplainString() + "expressionの後ろは ')' です");
			}

		} else if (AddressToValue.isFirst(tk)) {
			unsignedFactor = new AddressToValue(pcx);
			unsignedFactor.parse(pcx);
		}else {
			pcx.fatalError("Factorクラスでエラー");
		}
	}

	public void semanticCheck(CParseContext pcx) throws FatalErrorException {
		if (unsignedFactor != null) {
			unsignedFactor.semanticCheck(pcx);
			this.setCType(unsignedFactor.getCType());		// unsignedFactor の型をそのままコピー
			this.setConstant(unsignedFactor.isConstant());
		}
	}
	public void codeGen(CParseContext pcx) throws FatalErrorException {
		PrintStream o = pcx.getIOContext().getOutStream();
		o.println(";;; unsignedFactor starts");
		if (unsignedFactor != null) { unsignedFactor.codeGen(pcx); }
		o.println(";;; unsignedFactor completes");
	}

}