// 識別子に関する意味解析テスト（構文解析のテストは、各自が行うこと）
//
// ident.javaのsemanticCheckメソッドのところは、こうなっているはず
//
// public void semanticCheck(CParseContext pcx) throws FatalErrorException {
//      this.setCType(CType.getCType(CType.XXXXX));                                             //    \
  //this.setConstant(YYYYY);
// }

// (1) XXXXX を整数型 T_int に、YYYYYをfalseにして
//a       // 正当（生成コードが正しいかどうかも確認）
//*a      // 不当
//a[3]    // 不当
//&a
//
//// (2) XXXXX をポインタ型 T_pint に、YYYYYをfalseにして
//a    // 正当
//*a      // 正当（生成コードが正しいかどうかも確認）
//a[3]    // 不当（Ｃでは正当だが、この実験では不当にすること）
//
//// (3) XXXXX を配列型 T_array（人によってこの名前は異なる）に、YYYYYをfalseにして
////  ＊＊＊＊＊＊＊＊＊＊＊＊＊ 2018/10/26 変更 ＊＊＊＊＊＊＊＊＊＊
//a       // 不当（10/26より前は正当となっていた）
//*a      // 不当（Ｃでは正当だが、この実験では不当にすること）
//a[3]    // 正当（生成コードが正しいかどうかも確認）
//a[a[3]] // 正当（11/2追加）
//a[&3]   // 不当（[]内は整数型でなければならない）
//
//// (4) XXXXX をポインタ配列型 T_parray（人によってこの名前は異なる）に、YYYYYをfalseにして
////  ＊＊＊＊＊＊＊＊＊＊＊＊＊ 2018/10/26 変更 ＊＊＊＊＊＊＊＊＊＊
//a       // 不当（10/26より前は正当となっていた）
//*a      // 不当（Ｃでは正当だが、この実験では不当にすること）
//a[3]    // 正当
//a[3]+a[3] // 不当（11/2追加）
//*a[3]    // 正当（生成コードが正しいかどうかも確認）
//a[&3]   // 不当（[]内は整数型でなければならない）
//
//// (5)
//&*var   // 不当

//構文解析テスト
//*abc
//*abc123abc
//*abc[1+1]
//abc
//abc123abc
//abc123abc[0+9-7+&a+&a-888]
//abc123abc[a]
//*abc123abc[0+9-7+&a+&a-888] + *abc


//$*varはGCCではコンパイル通った。
//#include <stdio.h>
//int main(void){
//    // Your code here!
//    int i=3;
//    int* var;
//    var = &i;
//    printf("%d\n", &i);
//    printf("%d\n", var);
//    printf("%d", &*var);
//}
//

//型の演算テスト
//#include <stdio.h>
//const int MAX = 3;
//int main(void){
//    // Your code here!
//    int i=2,i2;
//    int* j,j2;
//    int k[2]={0,0}; int k2[2]={0,0};
//    int* l[2], l2[2];
//
//   //int[]-hoge
//   //printf("%d",k*i);
//   //printf("%d",k*j);
//   //printf("%d",k*k2);
//   //printf("%d",k*l);
//
//   //hoge - int[]
//   //printf("%d",i*k);
//   //printf("%d",j*k);
//   //printf("%d",l*k);
//
//   //int*[]-hoge
//   //printf("%d",l*i);
//   //printf("%d",l*j);
//   //printf("%d",l*k);
//   //printf("%d",l*l2);
//
//   //hoge-int*[]
//   //printf("%d",i*l);
//   //printf("%d",j*l);
//   printf("%d",k*l);
//
//   //足し算引き算も符号を変えて確認する
//   return 0;
//}